console.log("Hello World!");


// SECTION - Exponent Operator
let firstNum = Math.pow(8,2);
console.log(firstNum);

// es6
/*
	let/const = variableName = number ** exponent
*/

let secondNum = 8 ** 2;
console.log(secondNum);


// SECTION - Template literals
/*
	${variableName}

	- allows to write strings w/o using the concatination operator (+)
	- greatly improves code readability
*/

let name = "John";

// pre es6
let message = "Hello " + name + "! Welcome to programming!";
// Message without template literals: message
console.log('Message without template literals: ' + message);

// es6
message = `Hello ${name}! Welcome to programming!`
console.log(`Message with template literals: ${message}`)



// multiple line
const anotherMessage = `
${name} won the Math competition.
He won it by solving the problem 8 ** 2 with the solution of ${secondNum}.
He won it by solving the problem 8 ** 2 with the solution of ${8 ** 2}.
`;

console.log(anotherMessage);


/*
	MINIACTIVITY
		create 2 variables 
			interestRate which has a value of .15;
			principal = 1000

		log in the console the total interest of the account that has the principal as its balance and the interestRate as its interest%
*/

let interestRate = 0.15;
let principal = 1000;


console.log(`Principal balance is ${principal}`);
console.log(`With an interest rate of: ${interestRate}`);
console.log(`Total balance is ${principal + (principal*interestRate)}`);


// ${} can include mathematical operators




// SECTION - Array Destructuring
/*
	-allows us to unpack elements in arrays to distinct variables
	-allows us to name array elements with variables instead of using index numbers
	-helps with code readability

	SYNTAX:
		let/const [variableA, variableB, variableC] = arrayName;

*/

const fullName = ['Juan', 'Dela', 'Cruz'];
// pre-ES6
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);


//ES6
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

// using template literals
console.log(`Hello ${firstName} ${middleName} ${lastName}`);


/*
	MINIACTIVITY
		create a "person" object
			firstName = Jane
			middleName = Dela
			lastName = Cruz
		log in the console each of the properties and a greeting message for the person
*/



// SECTION - Object Destructuring
/*
	-allows us to unpack properties of objects into distinct variables
	-shortens the syntax for accessing the properties of an object

	SYNTAX
		let/const {propertyA, propertyB, propertyC} = objectName;
*/

let person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
};

//pre ES6
console.log(person);
console.log(`Greeting and salutations, ${person.firstName} ${person.middleName} ${person.lastName}`);



// ES6

const { givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);


// using object destructuring as a parameter of a function
function getFullName({givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${fullName}`)
};


getFullName(person);


/*
	MINIACTIVITY
		create a pet object
			name
			trick
			treat
		create a fxn that will receive an object destructuring as its parameter
			log in console "name, trick!"
			log in console "good trick!"
			log in console "here is a treat for you!"
*/

let pet ={
	petName: "Stephie",
	petTrick: "Sit",
	petTreat: "Eggnog"
}

function getPetName({petName, petTrick, petTreat}){
	console.log(`${petName}, ${petTrick}!`);
	console.log(`Good ${petTrick}!`);
	console.log(`Here is ${petTreat} for you!`);

}

getPetName(pet);


// SECTION Arrow Function
/*
	- a compact alternative syntax to traditional functions
*/


/*
const hello = ()=>{
	console.log("Hello World!");
};
*/

//pre ES6
/*
function printFullName (firstName, middleInitial, lastName){
	console.log(`${firstName} ${middleInitial} ${lastName}`);
};
*/

// Arrow function in ES6
/*
	SYNTAX
		let/const variableName = (parameterA, parameterB, parameterC) => {
			console.log(); statements/expressions
		}
*/

const printFullName = (firstName, middleInitial, lastName) =>{
	console.log(`${firstName} ${middleInitial} ${lastName}`)
}
printFullName("Portgas","D.","Ace");

printFullName("Naruto", "S.", "Uzumaki");



// Arrow function with loops
// pre-arrow function

const students = ["John", "Jane", 'Judy'];
students.forEach(function(student){
	console.log(`${student} is a student.`);
})

	
/*
	MINIACTIVITY
		create an arrow fxn with "add" as its name
		2 parameters x,y
		statement: using the return keyword, add x,y
	log in console the fxn value once it is called with proper arguments.
	
*/

// SECTION - implicit return statement
/*
	-shortens syntax requirements
*/

// pre ES6
let axis = (x,y) =>{
	return (x+y);
}
console.log(axis(5,10));

// arrow function
const add = (x,y) => x+y;
console.log(add(5,10));



/*
	the name = "User" sets the default value for the function greet() once it is called without any parameters
*/
const greet = (name = "User") => {
	return `Good morning ${name}!`;
}

console.log(greet("John"));
// console.log(greet());
	// this would return "Good morning User"




// SECTION - class-based Object Blue-prints
/*
	Notes:
		- allows creation/instantiation of objects using class as a blueprint


	Creating a class
		- "constructor" is a special method of a class for creating/initializing an object for that class
		- "this" keyword refers to the properties of an object create from the class; this allows us to reassign values for the properties inside the class.


	SYNTAX:
		class className{
			constructor(objectPropertyA, objectPropertyB, objectPropertyC){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
				this.objectPropertyC = objectPropertyC;
			}
		}
	

*/
class Car{
	constructor(brand,name,year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
};


/*
	SYNTAX:
		let/const variableName = new ClassName();
*/
// first instance
// dot notation for the object properties
let car = new Car();
car.brand = "Ford";
car.name = "Ranger Raptor";
car.year = 2015;

console.log(car);

// 2nd instance
// using arguments
const myCar = new Car("Toyota",'Vios',2017);
console.log(myCar);

