console.log("Hello World!");

// getCube formula
let x = 2 ;
let getCube = x ** 3 ;

// Log in console
console.log(`The cube of ${x} is ${getCube}`);


// Address
let address = [258, 'Washington Ave, NW', 'California', 90011];
let [houseNumber, street, state, zipCode] = address;

console.log(`I live at ${houseNumber} ${street} ${state} ${zipCode}`);

// animal object
let animal = {
	name: "Lolong",
	type: 'Saltwater Crocodile' ,
	weight: 1075,
	measurement: '20ft 3in' ,
};

const {name, type, weight, measurement} = animal;
console.log(`${name} is a ${type}. He weighs at ${weight}kgs with a measurement of ${measurement}`);


// number Array
let numArray = [1,2,3,4,5];

numArray.forEach((num) => {
	console.log(num);
});



//reduce number


let reduceNumber = numArray.reduce(function(x,y){
	return x + y; 
	});

console.log(reduceNumber);


// Class Constructor Dog
class Dog{
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};

let myDog = new Dog("Stephy",'7','Shitzu');
console.log(myDog);